function limpiar() {
    document.getElementById("Tfinanciar").value = "";
    document.getElementById("Valor").value = "";
    document.getElementById("enganche").value = "";
    document.getElementById("Pmensual").value = "";
}

function calcular() {
    plazo = 0;
    va = document.getElementById('Valor').value;
    en = document.getElementById('enganche').value = va * 0.30;
    tabla = document.getElementById('tab').value;

    re = va - en;

    if (tabla == 12) {
        plazo = 1.125
    } else if (tabla == 18) {
        plazo = 1.172
    } else if (tabla == 24) {
        plazo = 1.21
    } else if (tabla == 36) {
        plazo = 1.26
    } else {
        plazo = 1.45
    }

    document.getElementById('Tfinanciar').value = re * plazo;
    Tfinanciar = document.getElementById('Tfinanciar').value;

    document.getElementById('Pmensual').value = Tfinanciar / tabla;

}